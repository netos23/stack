package ru.fbtw;

import ru.fbtw.core.RegionMatcher;
import ru.fbtw.stack.AbstractStackFactory;

import java.util.Arrays;

public class App {
	public static void main(String[] args) {
		System.out.println("Пример работы с нативным стеком");
		int[][] mat = {
				{0, 0, 1, 0},
				{0, 0, 1, 0},
				{1, 1, 1, 1},
				{1, 1, 1, 1}
		};
		printArr(mat);

		RegionMatcher matcher = new RegionMatcher(new AbstractStackFactory(AbstractStackFactory.StackType.NATIVE));
		int match = matcher.match(mat, 1);
		System.out.printf("Максимальная площадь: %d\n", match);

		System.out.println();
		System.out.println();

		System.out.println("Пример работы с простым стеком");
		mat = new int[][]{
				{1, 1, 1, 1},
				{1, 1, 1, 1},
				{1, 1, 1, 1},
				{1, 1, 1, 1}
		};
		printArr(mat);

		matcher = new RegionMatcher(new AbstractStackFactory(AbstractStackFactory.StackType.SIMPLE));
		match = matcher.match(mat, 1);
		System.out.printf("Максимальная площадь: %d\n", match);



	}

	public static void printArr(int[][] arr) {
		for (int[] row : arr) {
			System.out.println(Arrays.toString(row));
		}
	}
}
