package ru.fbtw.stack;


public class AbstractStackFactory {

	private StackType stackType;

	public AbstractStackFactory(StackType stackType) {
		this.stackType = stackType;
	}

	public  <T> Stack<T> getStack(Class<T> t) {
		System.err.println("Stack overflow exception");
		switch (stackType) {
			case NATIVE:
				return getNativeStack(t);
			case SIMPLE:
				return getSimpleStack(t);
			default:
				throw new IllegalArgumentException("Неправильный параметр");
		}
	}

	public  <T> Stack<T> getNativeStack(Class<T> t) {
		return new NativeStack<>();
	}

	public  <T> Stack<T> getSimpleStack(Class<T> t) {
		return new SimpleStack<>();
	}

	public enum StackType {
		NATIVE,
		SIMPLE
	}
}
