package ru.fbtw.stack;

import java.util.Collection;

public interface Stack<E>  extends Collection<E> {
	void push(E element);
	E pop();
	E peek();
	boolean isEmpty();
}
