package ru.fbtw.stack;


import java.util.Collection;
import java.util.Iterator;

public class NativeStack<E> implements Stack<E> {
	private final java.util.Stack<E> stack;

	public NativeStack() {
		stack = new java.util.Stack<>();
	}

	@Override
	public void push(E element) {
		stack.push(element);
	}

	@Override
	public E pop() {
		return stack.pop();
	}

	@Override
	public E peek() {
		return stack.peek();
	}

	@Override
	public int size() {
		return stack.size();
	}

	@Override
	public boolean isEmpty() {
		return stack.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return stack.contains(o);
	}

	@Override
	public Iterator<E> iterator() {
		return stack.iterator();
	}

	@Override
	public Object[] toArray() {
		return stack.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return (T[]) stack.toArray(a);
	}

	@Override
	public boolean add(E e) {
		return stack.add(e);
	}

	@Override
	public boolean remove(Object o) {
		return stack.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return stack.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		return stack.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return stack.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return stack.retainAll(c);
	}

	@Override
	public void clear() {
		stack.clear();
	}
}
