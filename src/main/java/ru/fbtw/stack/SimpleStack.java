package ru.fbtw.stack;

import java.util.Collection;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimpleStack<E> implements Stack<E> {

	private final Node tail;
	private int size;

	public SimpleStack() {
		tail = new Node(null, null);
	}

	@Override
	public void push(E element) {
		size++;
		tail.prev = new Node(tail.prev, element);
	}

	@Override
	public E pop() {
		E data = peek();
		tail.prev = tail.prev.prev;
		size--;
		return data;
	}

	@Override
	public E peek() {
		if (!isEmpty()) {
			return tail.prev.data;
		} else {
			throw new EmptyStackException();
		}
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			Node cur;

			void init() {
				cur = tail;
			}

			@Override
			public boolean hasNext() {
				if (cur == null) {
					init();
				}

				return cur.prev != null;
			}

			@Override
			public E next() {
				if (cur == null) {
					init();
				}
				if (!hasNext()) {
					throw new NoSuchElementException();
				}

				cur = cur.prev;
				return cur.data;
			}
		};
	}

	@Override
	public Object[] toArray() {
		Object[] arr = new Object[size];
		final int[] index = {0};
		forEach(el -> arr[index[0]++] = el);
		return arr;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean add(E e) {
		push(e);
		return false;
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		c.forEach(this::push);
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		tail.prev = null;
		size = 0;
	}


	class Node {
		private Node prev;
		private final E data;

		public Node(Node prev, E data) {
			this.prev = prev;
			this.data = data;
		}
	}
}
