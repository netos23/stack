package ru.fbtw.core;

import ru.fbtw.stack.AbstractStackFactory;
import ru.fbtw.stack.Stack;

public class RegionMatcher {


	private final AbstractStackFactory factory;

	public RegionMatcher(AbstractStackFactory factory) {
		this.factory = factory;
	}

	public int match(int[][] mat, int target) {
		int[] histogram = new int[mat.length + 2];
		int res = 0;
		for (int i = 0; i < mat[0].length; i++) {
			getHistogram(mat, histogram, i, target);
			res = Math.max(searchInHistogram(histogram), res);
		}
		return res;
	}

	private int searchInHistogram(int[] histogram) {
		int res = 0;
		Stack<Integer> stack = factory.getStack(Integer.class);
		stack.push(0);
		for (int i = 0; i < histogram.length; i++) {
			while (!stack.isEmpty() && histogram[i] < histogram[stack.peek()]) {
				int h = histogram[stack.pop()];
				int area = h * (i - stack.peek() - 1);
				res = Math.max(area, res);
			}
			stack.push(i);
		}

		return res;
	}

	private void getHistogram(int[][] src, int[] hist, int c, int target) {
		for (int r = 1; r < hist.length - 1; r++) {
			int tmp = src[r - 1][c] == target ? 1 : 0;
			if (tmp > 0) {
				tmp += hist[r];
			}
			hist[r] = tmp;
		}
	}


}
