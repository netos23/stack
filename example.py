def largestRectangleArea(A):
    ans = 0
    A = [-1] + A
    A.append(-1)
    n = len(A)
    stack = [0]  # store index

    for i in range(n):
        while A[i] < A[stack[-1]]:
            h = A[stack.pop()]
            area = h * (i - stack[-1] - 1)
            ans = max(ans, area)
        stack.append(i)
    return ans


if __name__ == '__main__':
    a = [2, 3, 2, 3]
    print(largestRectangleArea(a))
